# Part 4 - Adding Transactional Behavior
## Behavior Definition
First, we have to create a behavior definition using the Behavior Definition Language (BDL) referring to a CDS data model’s root entity. This behavior definition is also handling all child entities that are part of the composition tree. All supported transactional operations must be specified in a single behavior definition.

### Introduction to BDL syntax

```abap
/*Header of behavior definition */  
[implementation] {unmanaged | managed | abstract};
with draft;

/* Definition of entity behavior */  
define behavior for CDSEntity [alias AliasName]

/* Entity properties */
[implementation in class ClASS_NAME unique]  
[persistent table DB_TABLE] 
[draft table DB_TABLE_D] 
[late numbering]  
[etag (field)]  
[lock {master | dependent (PropertyDependent = PropertyMaster)}]

{
 /* Static field control */  
 [field (readonly | mandatory) field1[, field2, ..., fieldn];]

 /* Standard operations */  
 [internal] create;                  
 [internal] update;   
 [internal] delete; 

 /* Actions */   
 [static] action ActionName   
                 [parameter {InputParameterEntity | $self)}]   
                 [result [cardinality] {OutputParameterEntity | $self}]; 
      
/* Associations */   
 association AssociationName [abbreviation AbbreviationName] {[create; with draft;]}
```

### Implementing the behavior definition
Now let’s start implementing the transactional behavior for our scenario: We’ll begin by creating a behavior definition using the Behavior Definition Language (BDL).
Behavior definitions are created only from the root cds view and define the behavior for all contains entities.
To create the behavior definition you have to do the following:

- Right-click on the CDS View of our root entity `ZRAPH_##_I_TravelWDTP` in the Project Explorer and choose New Behavior Definition

- Project, Package and Root Entity have been assigned automatically. The name of the behavior definition has to be the same as the root CDS view, that's why it is readonly

- Make sure the implementation type is `managed`, you can change the default description if you like then choose Next

- Assign a transport request and choose Finish

The behavior definition is created and defaulted based on the implementation type.

**Remark** When using the implementation type managed, for some features, you only need to define behavior characteristics and standard operations in the behavior definition. The RAP managed runtime framework provides a generic solution for
`create` , `update`, `delete`, `create by association`, `lock handling`, `ETag handling`.  
For a managed business object with draft, the following considerations are relevant:

- The behavior of each entity is implemented in a separate behavior class pool. Hence, the implementation class must be created for each entity separately. For more information, see  [Best Practices for Modularization and Performance](https://help.sap.com/viewer/fc4c71aa50014fd1b43721701471913d/202009.000/en-US/eb24e3f641df4dd99975f6155dcc6dfb.html#loioeb24e3f641df4dd99975f6155dcc6dfb__best_practices_mod).

- The managed implementation type requires the specification of lock master or lock dependent on each entity. For more information, see [Pessimistic Concurrency Control (Locking)](https://help.sap.com/viewer/fc4c71aa50014fd1b43721701471913d/202009.000/en-US/99d8162b8d7d4a83ae65320d2a03b8ab.html).

- In managed business objects, it is best practice to define a local ETag master on each entity. With an ETag master on every entity, you ensure that the ETag check is done for every entity independently. For more information, see [Optimistic Concurrency Control](https://help.sap.com/viewer/fc4c71aa50014fd1b43721701471913d/202009.000/en-US/41d72e9f31964082a7e7189f832010c3.html).

- The RAP managed runtime framework is able to automatically draw primary key values in UUID scenarios. You use this functionality by defining early managed numbering in the behavior definition. Setting the primary key field to read only defines strict internal numbering. An external BO consumer is not allowed to provide the primary key values in this case. For more detailed information, see [Automatically Drawing Primary Key Values in Managed BOs](https://help.sap.com/viewer/fc4c71aa50014fd1b43721701471913d/202009.000/en-US/82ce57d6d2014007944a667dcdb830bf.html).

- If different names are used on the database table and in the CDS data model, you need to define the mapping for these fields. This is done via the mapping operator in the behavior definition. 


#### Adjust behavior of `Travel` root entity.
1. Add an alias to Travel entity, uncomment `alias <alias_name>`, add `Travel` as alias.
2. Specify the database table name, uncomment `persistent table <db travel name>` and add the travel database here.
3. Enable the lock handling for the Travel entity which is the root node of the composition, to do that uncomment the line `lock master`.  
> **Remark** In managed scenarios the lock is handled automatically by the RAP framework.  
4. Activate Etag handling for the travel entity (optimistic lock) via the etag master statement and replace the `<field_name>` to `LocalLastChangedAt`:
```abap
etag master LocalLastChangedAt
```
5. Generate UUID: we want to have automatically generated UUIDs (Travel UUID field) every time new instances are created.  
To achieve this, specify the key field `TravelUUID` to be fully managed by the runtime and not editable from outside using the keywords `numbering:managed` and `readonly`.     
Add the new statement provided below:
```abap
  field ( numbering : managed, readonly ) TravelUUID;
```
6. Define a mapping between the persistency table fields and the CDS view fields for the Travel entity.  
> **Remark** Because we have provided aliases in the interface CDS views, we need to tell the framework how to map the element names in the CDS data model to the corresponding table fields:
```abap
  mapping for <travel db name>
  {
    <cds_field_name> = <db_field_name>
    ...
  }
```

#### Adjust behavior of sub-entities.
First we will adjust the booking entity. The same aproach would be for each sub-entities.

1. Maintain the alias for Booking, uncomment  `alias <alias_name>` replace `<alias_name>` with Booking.
2. Specify the database table as persistency by uncommenting the line `persistent table <db_name>` and add the correct database table name.
3. Enable the lock handling for the Booking entity which is a child node in the composition model – Travel being the lock master as the root entity. For this reason, the booking entity is lock dependent and makes use of the `_Travel` association defined in the appropriate CDS view.
For that, uncomment the statement `line lock dependent by` and replace the entry `<association>` with `_Travel`.
```abap
 lock dependent by _Travel
```
4. Enable the so-called optimistic lock for the Booking entity.
For that, uncomment the etag master statement and replace the `<field_name>` to LocalLastChangedAt in it.
```abap
etag master LocalLastChangedAt
```
> **Remark**
> Defining two ETag masters happens on purpose. The recommended approach is to have a local etag for each entity. This is achieved by specifying an etag master on each node.

5. In order to transactionally enable the `_Travel` association explicitly add it in the list between the curly brackets. You can add it at the top.
```abap
  association _Travel; 
```
6. Generate UUID: we want to have automatically generated UUIDs (Booking UUID field) every time new instances are created.
To achieve this, specify the key field `BookingUUID` to be fully managed by the runtime and not editable from outside using the keywords `numbering:managed` and `readonly`.  
Add the new statement like you did for the travel entity before.

7. Make `TravelUUID` field readonly `field( readonly ) TravelUUID`.

8. Define a mapping between the persistency table fields and the CDS view fields for the Booking entity.  
 Because we have provided aliases in the interface CDS views, we need to tell the framework how to map the element names in the CDS data model to the corresponding table fields.
```abap
  mapping for <booking db name>
  {
    <cds field name>         = <db_field_name>;
    ...
  }
```
>Repeat the sub-entity steps from above for both BookingSupplement and RoomReservation.

**Solution** 
- [ZRAPH_##_I_TravelWDTP](sources/Z_I_TravelWDTP.txt) without draft.

## Next step
[4b. Enabling draft and projecting behavior definition](4b.md)