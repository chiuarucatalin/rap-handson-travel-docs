METHOD validatedates.
  "Read relevant travel instance data
  READ ENTITIES OF zraph_##_i_travelwdtp IN LOCAL MODE
  FIELDS ( begindate enddate ) WITH CORRESPONDING #( keys )
    RESULT DATA(lt_travel).

  LOOP AT lt_travel INTO DATA(ls_travel).

    APPEND VALUE #(  %tky               = ls_travel-%tky
                     %state_area        = 'VALIDATE_DATES' ) TO reported-travel.

    IF ls_travel-begindate IS INITIAL.
      APPEND VALUE #( %tky               = ls_travel-%tky ) TO failed-travel.
      APPEND VALUE #( %tky               = ls_travel-%tky
                      %state_area        = 'VALIDATE_DATES'
                      %msg               =  new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                         number   = '003' " Enter Begin Date for travel
                                                         v1       = ls_travel-travelid
                                                         severity = if_abap_behv_message=>severity-error )
                      %element-begindate = if_abap_behv=>mk-on ) TO reported-travel.
    ENDIF.
    IF ls_travel-enddate IS INITIAL.
      APPEND VALUE #( %tky               = ls_travel-%tky ) TO failed-travel.
      APPEND VALUE #( %tky               = ls_travel-%tky
                      %state_area        = 'VALIDATE_DATES'
                      %msg               =  new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                         number   = '004' " Enter EndDate for travel
                                                         v1       = ls_travel-travelid
                                                         severity = if_abap_behv_message=>severity-error )
                      %element-enddate   = if_abap_behv=>mk-on ) TO reported-travel.
    ENDIF.

    IF ls_travel-enddate < ls_travel-begindate
      AND ls_travel-begindate IS NOT INITIAL
      AND ls_travel-enddate IS NOT INITIAL.

      APPEND VALUE #( %tky               = ls_travel-%tky ) TO failed-travel.
      APPEND VALUE #( %tky               = ls_travel-%tky
                      %state_area        = 'VALIDATE_DATES'
                      %msg               = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                        number   = '001'
                                                        v1       = ls_travel-begindate
                                                        v2       = ls_travel-enddate
                                                        v3       = ls_travel-travelid
                                                        severity = if_abap_behv_message=>severity-error )
                      %element-begindate = if_abap_behv=>mk-on
                      %element-enddate   = if_abap_behv=>mk-on ) TO reported-travel.

    ELSEIF ls_travel-begindate < cl_abap_context_info=>get_system_date( )
      AND ls_travel-begindate IS NOT INITIAL
      AND ls_travel-enddate IS NOT INITIAL.

        APPEND VALUE #( %tky               = ls_travel-%tky ) TO failed-travel.
        APPEND VALUE #( %tky               = ls_travel-%tky
                        %state_area        = 'VALIDATE_DATES'
                        %msg               = new_message( id       = 'ZRAPH_MSG_TRAVELWD'
                                                          number   = '002' "Begin date &1 must be on or after system date
                                                          v1       = ls_travel-begindate
                                                          severity = if_abap_behv_message=>severity-error )
                        %element-begindate = if_abap_behv=>mk-on ) TO reported-travel.
    ENDIF.
  ENDLOOP.
ENDMETHOD.