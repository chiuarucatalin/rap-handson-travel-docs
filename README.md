# Welcome to the Hands-On - ABAP RESTful Application Programming Model (RAP) - Scenario Managed/Draft

This repository offers hands-on exercises on RAP provided by msg group.

## General
### Hands-On - Prerequisites

* Minimum system prerequisite **SAP S/4HANA 2020 FPS01**
* [Installation Guide: SAP Trial, Eclipse and Business Application Studio](installation/Installation.md)
* Basic knowledge of ABAP & ABAP CDS Views 
  
### SAP Netweaver
All created objects are implemented as local objects. The names to be used are listed within the corresponding chapters. For all objects, make sure, that you also provide the initials of your name as prefix. 

## "The App"
### Goals and Motivation
The main goal of this exercise is to show how software development with ABAP has changed within the past years. The exercise wants to highlight the future techniques and technologies within the **ABAP RESTful Application Programming Model** (you may also find the abbreviation **RAP**). 

Simplification and fast IT was the leading motivation for SAP in the last years – this brought several new functionalities and features. This HandsOn aims to make you familiar with those new technologies by build-ing your own app. When you finished this exercise, you have basic knowledge how a simple Fiori App can be created with the help of CDS Views (data retrieval and UI annotation), Behavior Definitions (transactional actions on data) and the resulting OData service (Service Binding).

### SAP Fiori transactional app (RAP scenario managed/draft)
#### 1. Introduction
Find more details in folder [part1](part1/README.md) of this Git repository.
#### 2. Creating the Database Tables
Find more details in folder [part2](part2/README.md) of this Git repository.
#### 3. Creating the Virtual Data Model (VDM) via ABAP CDS Views
Find more details in folder [part3](part3/3a.md) of this Git repository.
#### 4. Behavior Definition & Projection
Find more details in folder [part4](part4/4a.md) of this Git repository.
#### 5. Publishing the Business Service
Find more details in folder [part5](part5/README.md) of this Git repository.
#### 6. Adding Transactional Behavior
Find more details in folder [part6](part6/6a.md) of this Git repository.
#### 7. Creating the SAP Fiori List Report app
Find more details in folder [part7](part7/README.md) of this Git repository.
#### 8. Deploying your app
Find more details in folder [part8](part8/README.md) of this Git repository.
#### 9. Extending the SAP Fiori List Report
Find more details in folder [part9](part9/README.md) of this Git repository.
## License
Copyright (c) 2021 msg group. All rights reserved. This project is licensed under the Apache Software License, version 2.0 except as noted otherwise in the [LICENSE](LICENSES/Apache-2.0.txt) file.