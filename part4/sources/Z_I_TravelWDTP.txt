managed;

define behavior for ZRAPH_##_I_TRAVELWDTP alias Travel
persistent table ZRAPH_##_Travel
lock master
etag master LocalLastChangedAt
{
  create;
  update;
  delete;
  association _Booking { create; }
  association _RoomReservation { create; }

  field ( numbering : managed, readonly ) TravelUUID;

  mapping for ZRAPH_##_Travel
  {
    TravelUUID = travel_uuid;
    TravelID = travel_id;
    AgencyID = agency_id;
    CustomerID = customer_id;
    BeginDate = begin_date;
    EndDate = end_date;
    BookingFee = booking_fee;
    TotalPrice = total_price;
    CurrencyCode = currency_code;
    Description = description;
    OverallStatus = overall_status;
    LocalCreatedBy = local_created_by;
    LocalCreatedAt = local_created_at;
    LocalLastChangedBy = local_last_changed_by;
    LastChangedAt = last_changed_at;
    LocalLastChangedAt = local_last_changed_at;
  }
}


define behavior for ZRAPH_##_I_BOOKINGWDTP alias Booking
persistent table ZRAPH_##_Booking
lock dependent by _Travel
etag master LocalLastChangedAt
{

  association _Travel;

  update;
  delete;
  association _BookingSupplement { create; }

  field ( numbering : managed, readonly ) BookingUUID;
  field ( readonly ) TravelUUID;

  mapping for ZRAPH_##_Booking
  {
    BookingUUID = booking_uuid;
    TravelUUID = parent_uuid;
    BookingID = booking_id;
    BookingDate = booking_date;
    CustomerID = customer_id;
    CarrierID = carrier_id;
    ConnectionID = connection_id;
    FlightDate = flight_date;
    FlightPrice = flight_price;
    CurrencyCode = currency_code;
    LocalLastChangedAt = local_last_changed_at;
  }

}

define behavior for ZRAPH_##_I_BOOKINGSUPPLWDTP alias BookingSupplement
persistent table ZRAPH_##_BookSup
lock dependent by _Travel
etag master LocalLastChangedAt
{
  association _Travel;
  update;
  delete;

  field ( numbering : managed, readonly ) BookSupplUUID;
  field ( readonly ) TravelUUID, BookingUUID;

    mapping for ZRAPH_##_BookSup
  {
    BookSupplUUID = booksuppl_uuid;
    TravelUUID = root_uuid;
    BookingUUID = parent_uuid;
    BookingSupplementID = booking_supplement_id;
    SupplementID = supplement_id;
    Price = price;
    CurrencyCode = currency_code;
    LocalLastChangedAt = local_last_changed_at;
  }
}

define behavior for ZRAPH_##_I_ROOMRESERVATIONWDTP alias RoomReservation
persistent table ZRAPH_##_RoomRsv
lock dependent by _Travel
etag master LocalLastChangedAt
{
  association _Travel;

  update;
  delete;

  field ( numbering : managed, readonly ) RoomRsvUUID;
  field ( readonly ) TravelUUID;

  mapping for ZRAPH_##_RoomRsv
  {
    RoomRsvUUID = roomrsv_uuid;
    TravelUUID = parent_uuid;
    RoomRsvID = roomrsv_id;
    HotelID = hotel_id;
    BeginDate = begin_date;
    EndDate = end_date;
    RoomType = room_type;
    RoomRsvPrice = roomrsv_price;
    CurrencyCode = currency_code;
    LocalLastChangedAt = local_last_changed_at;
  }
}