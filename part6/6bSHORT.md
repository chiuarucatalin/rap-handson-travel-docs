# Part 6b - Determinations & Validations

## Finalising the behavior implementation

### Determination
Determinations are used to determine, derive or calculate the fields of the instance in use. A determination is called based on some triggers conditions, for example it can be automatically executed when a create, update or delete of an instance is happening.
For more informations see [Determinations](https://help.sap.com/viewer/fc4c71aa50014fd1b43721701471913d/202009.000/en-US/c0a547a10ca04b1492945e9d8dc3e836.html). You can see the behavior definition syntax below:  
```abap
define behavior for ZRAPH_##_I_TravelWDTP alias Travel
...
{
...
  determination <doSomething> on modify { create; field <trigger_field_name> ; }
...
}
```

>**Remark**  You can only define trigger fields for a determination from the same entity the determination is assigned to. A determination that is defined for the travel entity cannot have trigger fields from e.g. the booking entity.

#### Implementating Determination `setInitialStatus`
We want to set the initial status of newly created travel instances to _Open (O)_. To achieve this, we'll need to add a determination on modify with trigger operation `create` to the behavior definition. This time, you **don't** have to `use` the determination in the behavior projection as determinations will always run automatically.

After creation, the overall status of the travel should only be changed by the action `acceptTravel`, which we previously created, therefore the field should be read-only for the external consumer.  

To implement this action you have to first add it to the behavior definition and then create a new method in the travel behavior pool via the Quick Fix. Use the EML like before to `MODIFY` the travel instance and update the overall status accordingly. This time, we don't have to return any data after our EML modification.  

**Solution**  
[ZRAPH_##_BP_I_TRAVELWDTP~setInitialStatus](sources/SetInitialStatus.txt)


### Validation
 
Validations are used to verify if the values added by the user are consistent, in case the values are wrong an error is raised with a relevant message, in this case the save is not done.   
For more informations see [Validations](https://help.sap.com/viewer/fc4c71aa50014fd1b43721701471913d/202009.000/en-US/abfbcd933c264fe4a4883d80d1e951d8.html). You can see the behavior definition syntax below:  
```abap
define behavior for ZRAPH_##_I_TravelWDTP alias Travel
...
{
...
  validation <doSomething> on save { create; field <trigger_field_name> ; }
...
}
```

>**Remark** Via a quick fix, you can generate the method declaration in the behavior pool directly from the behavior definition editor.

>**Important** Make sure you add the validation also to the `Prepare` framework action in the root behavior definition. This is necessary in draft scenarios to also run validations on draft entities and not only for final checks during activation. Please add the following statement to the travel root behavior definition - directly between the just created determination and the field mapping for ZRAPH_##_Travel:  
`draft determine action Prepare {`  
  `validation validateCustomer;`  
`}`


#### Implementating Validation `validateCustomer` 
Our first validation will check whether the customer chosen for this travel is actually existing and valid. Therefore you have to define a validation on save with trigger operation `create` using the trigger field `CustomerID`. Since there must always be a customer assigned to a certain travel, define the field `CustomerID` as mandatory in the behavior definition as well. This time, you **don't** have to `use` the validation in the behavior projection as validations will always run automatically. 

In the travel behavior pool you have to `READ` the `CustomerID` field for the created travels using the parameter `keys` in EML.  

Next we want to inner join the retrieved travel instances with the master data table `/DMO/Customer` into a new local table. Entries with invalid customer ids won't be included in this joined table telling us that missing travels have to have an invalid customer.  

Afterwards loop over the travels and check for every entry if the `CustomerID` has a value or if it `IS INITIAL`. If it has a value you have to check the validity of this (check if `line_exists( <local_table>[ customer_id = <current_travel>-customerid ])` results in `true`).

Once you implemented abovementioned logic you can check the solution to see how we have to react if the customer is either initial or invalid. In those cases we have to add the instance to the `failed` and `reported` tables and provide a error message (like shown in the solution).  

**Solution**  
[ZRAPH_##_BP_I_TRAVELWDTP~validateCustomer](sources/TravelValidateCustomer.txt)

## Next step
[6c. Feature Control](6c.md)