# Part 6b - Determinations

## Introducing Determination Definition Syntax
Determinations are used to determine, derive or calculate the fields of the instance in use. A determination is called based on some triggers conditions, for example it can be automatically executed when a create, update or delete of an instance is happening.
For more informations see [Determinations](https://help.sap.com/viewer/fc4c71aa50014fd1b43721701471913d/202009.000/en-US/c0a547a10ca04b1492945e9d8dc3e836.html). You can see the behavior definition syntax below:  
```abap
define behavior for ZRAPH_##_I_TravelWDTP alias Travel
...
{
...
  determination <doSomething> on modify { create; field <trigger_field_name> ; }
...
}
```

>**Remark**  You can only define trigger fields for a determination from the same entity the determination is assigned to. A determination that is defined for the travel entity cannot have trigger fields from e.g. the booking entity.

## Implementing Determinations

**1. Travel**

- Determination `setInitialStatus`: We want to set the initial status of newly created travel instances to 'Open'. To achieve this, we'll need to add a determination on modify with trigger operation `create` to the behavior definition. This time, you **don't** have to `use` the determination in the behavior projection as determinations will always run automatically.  
After creation, the overall status of the travel is only changed by the actions `rejectTravel` and `acceptTravel`, the two actions that we previously created, therefore the field should be read-only for the external consumer.  
To implement this action you have to first add it to the behavior definition and then create a new method in the travel behavior pool via the Quick Fix. Use the EML like before to `MODIFY` the travel instance and update the overall status accordingly. This time, we don't have to return any data after our EML modification.  
**Solution**  
[ZRAPH_##_BP_I_TRAVELWDTP~setInitialStatus](sources/SetInitialStatus.txt)

- Determination `calculateTotalPrice`: Previously, we've already implemented the internal action `ReCalcTotalPrice`on the travel instance. In order to react on modifications of all associated entities, this action has to be called internally whenever a change happens.  
For the travel instance itself this means we have to add a determination which reacts on changes to `BookingFee`and `CurrencyCode`.  
In the implementation of this determination you simply have to call the travel's internal action.  
**Solution**  
[ZRAPH_##_BP_I_TRAVELWDTP~calculateTotalPrice](sources/TravelCalculateTotalPrice.txt)

- Determination `setTravelID`: We want to automatically provide a TravelID for newly created instances without requiring the user to set one manually.  
We need an additional determination generating a new unique id for the `TravelID` field of Travel entity. This determination it should be on modify with trigger operation `create`. The user should not be able to modify this id afterwards, therefore the field should be set to read-only.  
For implementation we will go the way and simply read the highest currently used ID from the database and just use this ID increased by 1. In that case we'll first read the travels to be created with EML `READ` using the `keys` parameter from the RAP framework. Afterwards, loop over those entities, modify the `TravelID` accordingly and store the changes after the loop using EML's `MODIFY`.
    **Remark:** In productive scenarios, you should never rely on this simple scenario. If several instances are created at the same time, it might happen that both will be assign the same ID which will obviously result in errors. For this course we'll ignore this problem but in productive scenarios we would use a number range object which is basically a counter returning unique numbers one after another - for more information see [Maintaining a number range object](https://help.sap.com/saphelp_em92/helpdata/en/48/d58f92982b424be10000000a421937/content.htm?no_cache=true).  
    
    **Solution**  
[ZRAPH_##_BP_I_TRAVELWDTP~setTravelID](sources/SetTravelID.txt).

**2. Booking**

- Determination `calculateTotalPrice`: This basically follows the logic of the determinations in the other instances. Whenever a price or currency is changed, the internal action of the root entity to recalculate the total price should be triggered. For the booking this means fields `FlightPrice`and `CurrencyCode`as trigger conditions.  
For implementation, we'll first get the `LINK` relation to affected travel entities by reading `ENTITY booking BY \_travel` via EML. Afterwards, we execute the internal action `recalcTotalPrice` for the retrieved entities (looping over `LINK`).   
**Solution**  
[ZRAPH_##_BP_I_TRAVELWDTP~calculateTotalPrice](sources/BookingCalculateTotalPrice.txt)

- Determination `setBookingDate`: the `BookingDate` is set when the instance is saved, the value should not be changed afterwards. Therefore, set the field to read-only.  
For implementation we want to set the current system date (which we'll get via calling `cl_abap_context_info=>get_system_date( )` ) as `BookingDate` of newly created booking instances. First, read the affected entites via the `keys` parameter, loop over the retrieved instances afterwards and set the date where it's not yet set. Finally, save the changes of this particular field via EML.  
**Solution**  
[ZRAPH_##_BP_I_TRAVELWDTP~setBookingDate](sources/SetBookingDate.txt)

**3. Booking Supplement**

- Determination `calculateTotalPrice`: This basically follows the logic of the determinations in the other instances. Whenever a price or currency is changed, the internal action of the root entity to recalculate the total price should be triggered. For the booking supplement this means fields `Price`and `CurrencyCode`as trigger conditions.  
For implementation, we'll first get the `LINK` relation to affected travel entities by reading `ENTITY bookingsupplement BY \_travel` via EML. Afterwards, we execute the internal action `recalcTotalPrice` for the retrieved entities (looping over `LINK`).   
**Solution**  
[ZRAPH_##_BP_I_TRAVELWDTP~calculateTotalPrice](sources/BookingSupplCalculateTotalPrice.txt)  

**4. Room Reservation**

- Determination `calculateTotalPrice`: This basically follows the logic of the determinations in the other instances. Whenever a price or currency is changed, the internal action of the root entity to recalculate the total price should be triggered. For the room reservation this means fields `RoomRsvPrice`and `CurrencyCode`as trigger conditions.  
For implementation, we'll first get the `LINK` relation to affected travel entities by reading `ENTITY roomreservation BY \_travel` via EML. Afterwards, we execute the internal action `recalcTotalPrice` for the retrieved entities (looping over `LINK`).   
**Solution**  
[ZRAPH_##_BP_I_TRAVELWDTP~calculateTotalPrice](sources/RoomReservationCalculateTotalPrice.txt)

## Next step
[6c. Validations](6c.md)