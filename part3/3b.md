# Part 3b - Adding master data associations to the VDM
As a next step, we want to create further connections to the existing master data for Customer, Carrier, Agency, Currency, Supplement, SupplementText, Hotel and HotelRoomType as shown before. Those won’t be transactional or CRUD enabled. They are only necessary to access additional information not stored within Travel, Booking, BookingSupplement or RoomReservation instances themselves.  

The associations should be published in the CDS Views in order to access their fields when consuming the data. Here we won’t create our own CDS Views for sake of simplicity but will instead reuse the existing CDS views of the package /DMO/FLIGHT_REUSE provided by SAP and just refer to them.  

Please add join associations to the following CDS Views whereever possible: `/DMO/I_Agency`, `/DMO/I_Customer`, `/DMO/I_Carrier`, `/DMO/I_Connection`, `/DMO/Supplement`, `/DMO/I_SupplementText`, `ZRAPH_I_Hotel`, `ZRAPH_I_HotelRoomType`, `ZRAPH_I_OverallStatus` and `I_Currency`. You'll have to check the key fields of those CDS views (can be checked via `F2`) to know, how to join them. Again, publish those associations in the field list as well!  

#### Solutions

* [ZRAPH_##_I_TravelWDTP](sources/Z_I_TravelWDTP_EXT1.txt)
* [ZRAPH_##_I_BookingWDTP](sources/Z_I_BookingWDTP_EXT1.txt)
* [ZRAPH_##_I_BookingSupplWDTP](sources/Z_I_BookingSupplementWDTP_EXT1.txt)
* [ZRAPH_##_I_RoomReservationWDTP](sources/Z_I_RoomReservationWDTP_EXT1.txt)

## Checking result via Data Preview
Now after publishing the associations, the data preview functionality becomes more powerful. Once again, execute the data preview for the Travel CDS View. Mark one Travel instance by clicking on it and then press the small arrow at the top besides the name of the view. The system provides a list of all associations where you can drill into and see the data of all its associated items.

![alt](images/image3_6.png)

## Adding Criticality field directly within the CDS View
Next we want to add a new field for Criticality. This will be used to define certain thresholds on which a Booking instance is marked in red, yellow or green depending on its `FlightPrice`. For this, add the field `Criticality` with the case syntax to your view `ZRAPH_##_I_BookingWDTP`.  

The value 3 will be shown as red in the Fiori App while 2 is yellow, 1 is green and 0 is neutral (grey). You can choose some thresholds on your own, just make sure that your values make sense using the Data Preview.  

```abap
define view entity ZRAPH_##_I_BookingWDTP
  ...   	
      case when $projection.FlightPrice <  200 then 3
           when $projection.FlightPrice >= 200 and $projection.FlightPrice < 500 then 2
           when $projection.FlightPrice >= 500 then 1
           else 0
      end                         as Criticality,
  ...   	
```

## Next step
[3c. Projection Views](3c.md)