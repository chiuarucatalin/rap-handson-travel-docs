# Chapters

### [1. Domains](./Domains.md)

### [2. Data Elements](./DataElements.md)

### [3. Structures](./Structures.md)

### [4. Tables](./Tables.md)

### [5. Table Types](./TableTypes.md)

### [6. CDS View Entities](./CDSViewEntities.md)