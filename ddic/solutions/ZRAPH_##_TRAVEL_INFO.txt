@EndUserText.label : 'Travel Info'
@AbapCatalog.enhancement.category : #NOT_EXTENSIBLE
define structure zraph_##_travel_info {
  travel_id   : /dmo/travel_id;
  agency_id   : /dmo/agency_id;
  customer_id : /dmo/customer_id;
  begin_date  : /dmo/begin_date;
  end_date    : /dmo/end_date;
  dayamount   : abap.int4;

}