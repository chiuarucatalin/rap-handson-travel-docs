# Part 7c - Fiori Tools Capabilities


### SAP Fiori Tools
SAP Fiori tools simplifies the development of SAP Fiori elements applications by providing extensions for your SAP Business Application Studio and Visual Studio Code development environment.

The SAP Fiori tools extensions help you create applications, visualize navigation, automatically generate code, and more. Used in combination with SAP Fiori elements, these extensions can increase your development productivity, ensure the consistency of experience between applications, and help you build a scalable experience. 

![alt](images/image6_11.png)

SAP Fiori tools include the following extensions:

- **Application Wizard/Generator** - a wizard-style approach to generate the provided SAP Fiori elements and SAPUI5 freestyle floorplans. As you may observed, we used already the *Application Wizard* when we generated our SAP Fiori List Report app.
- **Application Modeler** - access to a visualization of the application pages, navigation, and service entities. You can add new navigation and pages, delete pages, and navigate to corresponding editing tools. The following features are part of this extension - *Page Editor* and *Page Map*.
- **Guided Development** - access to *How-To* guides and tutorials that explain how to implement certain functionality in an SAP Fiori elements application. You can follow the steps required to implement a feature and then use the guided development approach to make the required changes in your project.
- **Service Modeler** - visualization of the OData service metadata files. You can use it to browse complex services easily, including entities, properties, and associations.
- **XML Annotation Language Server** - access to resources that help to define annotations in the code editor, thus improving application development by reducing effort and maintaining code consistency. The following subset of features is part of this extension: Code completion, micro-snippets, diagnostics, internationalization support.

In the followin chapters of this hands-on, you will get yourself more in touch with each and every feature provided by SAP Fiori Tools.


## Optional
[8. Deploying your app](../part8/README.md)

## Next step
[9. Extending the SAP Fiori List Report](../part9/README.md)